def insertion_sort(lista):
    return lista


if __name__ == "__main__":
    li = [1, 9, 8, 5, 7, 3]

    print("Lista przed sortowaniem: {}".format(li))
    result = insertion_sort(li)
    print("Lista po sortowaniu: {}".format(result))

    assert result == [1, 3, 5, 7, 8, 9], "Lista nie jest posortowana!"
